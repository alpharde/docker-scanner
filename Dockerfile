FROM alpine:3.17
RUN apk add python3 py3-requests dumb-init
ADD run /usr/local/bin/run
ADD main /usr/local/bin/main
ADD thread-watcher /usr/local/bin/thread-watcher
ADD makehash /usr/local/bin/makehash
RUN chmod +x /usr/local/bin/run &&\
    chmod +x /usr/local/bin/main &&\
    chmod +x /usr/local/bin/thread-watcher &&\
    chmod +x /usr/local/bin/makehash
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["run"]
VOLUME /data
